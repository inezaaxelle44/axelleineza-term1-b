package com.example.axelleinezaterm1b.exception;

public class InvalidOperationException extends Exception {
    public InvalidOperationException(String message) {
        super(message);
    }
}

