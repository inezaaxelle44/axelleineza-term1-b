package com.example.axelleinezaterm1b.controller;

import com.example.axelleinezaterm1b.dto.DoMathRequest;
import com.example.axelleinezaterm1b.exception.InvalidOperationException;
import com.example.axelleinezaterm1b.service.MathOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/math")
public class MathController {

    private final MathOperator mathOperator;

    @Autowired
    public MathController(MathOperator mathOperator) {
        this.mathOperator = mathOperator;
    }

    @PostMapping("/doMath")
    public ResponseEntity<Map<String, Double>> doMath(@RequestBody DoMathRequest request) {
        try {
            double result = mathOperator.doMath(request.getOperand1(), request.getOperand2(), request.getOperation());
            Map<String, Double> response = Collections.singletonMap("calcResponse", result);
            return ResponseEntity.ok(response);
        } catch (InvalidOperationException e) {
            // Handle invalid operation exception
            Map<String, Double> response = Collections.singletonMap("calcResponse", 0.0);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }
}
