package com.example.axelleinezaterm1b.service;

import com.example.axelleinezaterm1b.exception.InvalidOperationException;

public interface MathOperator {
    double doMath(double operand1, double operand2, String operation) throws InvalidOperationException;
}

