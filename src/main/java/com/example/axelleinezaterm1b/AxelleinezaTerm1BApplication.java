package com.example.axelleinezaterm1b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxelleinezaTerm1BApplication {

    public static void main(String[] args) {
        SpringApplication.run(AxelleinezaTerm1BApplication.class, args);
    }

}
