package com.example.axelleinezaterm1b.e2e;

import com.example.axelleinezaterm1b.dto.DoMathRequest;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Value;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MathControllerEndToEndTest {

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testDoMath_ValidInput_ReturnsResult() {
        RestAssured.given()
                .port(port)
                .contentType(JSON)
                .body(new DoMathRequest(5, 4, "*"))
                .when()
                .post("/api/math/doMath")
                .then()
                .statusCode(200)
                .body("calcResponse", Matchers.closeTo(20.0, 0.001));
    }

    @Test
    public void testDoMath_InvalidInput_ReturnsBadRequest() {
        RestAssured.given()
                .port(port)
                .contentType(JSON)
                .body(new DoMathRequest(5, 0, "/"))
                .when()
                .post("/api/math/doMath")
                .then()
                .statusCode(400);
    }
}
