package com.example.axelleinezaterm1b.controller;

import com.example.axelleinezaterm1b.dto.DoMathRequest;
import com.example.axelleinezaterm1b.exception.InvalidOperationException;
import com.example.axelleinezaterm1b.service.MathOperator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)

public class MathControllerUnitTest {

    @Mock

    private MathOperator mathOperator;

    @InjectMocks
    private MathController mathController;

    @Test
    public void testDoMath_ValidInput_ReturnsResult() throws InvalidOperationException {
        // Mocking
        when(mathOperator.doMath(5, 4, "*")).thenReturn(20.0);

        // Test
        ResponseEntity<Map<String, Double>> responseEntity = mathController.doMath(new DoMathRequest(5, 4, "*"));

        // Assertions
        Assertions.assertEquals(HttpStatus.OK, ((ResponseEntity<?>) responseEntity).getStatusCode());
        Assertions.assertEquals(20.0, Objects.requireNonNull(responseEntity.getBody()).get("calcResponse"), 0.0);
    }

    @Test
    public void testDoMath_InvalidInput_ReturnsBadRequest() throws InvalidOperationException {
        // Mocking
        when(mathOperator.doMath(5, 0, "/")).thenThrow(new InvalidOperationException("Can't divide a number by 0"));

        // Test
        ResponseEntity<Map<String, Double>> responseEntity = mathController.doMath(new DoMathRequest(5, 0, "/"));

        // Assertions
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(0.0, Objects.requireNonNull(responseEntity.getBody()).get("calcResponse"), 0.0);
    }
}
