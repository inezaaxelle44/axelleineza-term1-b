package com.example.axelleinezaterm1b.integration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.closeTo;

@SpringBootTest
@AutoConfigureMockMvc
public class MathControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testDoMath_ValidInput_ReturnsResult() throws Exception {
        mockMvc.perform(post("/api/math/doMath")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"operand1\": 5, \"operand2\": 4, \"operation\": \"*\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.calcResponse", closeTo(20.0, 0.001)));
    }

    @Test
    public void testDoMath_InvalidInput_ReturnsBadRequest() throws Exception {
        mockMvc.perform(post("/api/math/doMath")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"operand1\": 5, \"operand2\": 0, \"operation\": \"/\"}"))
                .andExpect(status().isBadRequest());
    }
}
